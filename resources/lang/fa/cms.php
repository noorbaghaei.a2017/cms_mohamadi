<?php

return [

    'option'=>'گزینه ها',
    'add'=>'افزودن',
    'edit'=>'ویرایش',
    'delete'=>'حذف',
    'gallery'=>'گالری',
    'questions'=>'سوالات متداول',
    'detail'=>'جزییات',
    'create_date'=>'تاریخ ثبت',
    'update_date'=>'تاریخ بروز رسانی',
    'thumbnail'=>'تصویر شاخص',
    'title'=>'عنوان',
    'text'=>'متن',
    'excerpt'=>'خلاصه',
    'order'=>'الویت',
    'slug'=>'نامک',
    'sure_delete'=>' آیا از حذف  مطمئن هستید؟',
    'first_name'=>'نام',
    'last_name'=>'نام خانوادگی',
    'inventory'=>'موجودی',
    'code'=>'کد',
    'mobile'=>'موبایل',
    'email'=>'ایمیل',
    'countries'=>'کشور ها',
    'label'=>'برچسب',
    'create'=>'ایجاد',
    'seo'=>'سئو',
    'seo-services'=>'خدمات سئو',
    'keyword'=>'کلمات کلیدی',
    'description'=>'متن توضیحات',
    'create_article'=>'ایجاد مقاله',
    'create_information'=>'ایجاد خبر',
    'tags'=>'برچسب ها',
    'dashboard'=>'داشبورد ',
    'fast_access'=>'دسترسی سریع',
    'href'=>'آدرس',
    'version'=>'نسخه',
    'copy'=>'تمامی حقوق برای «امین نوربقایی » محفوظ است. ',
    'profile'=>'پروفایل ',
    'logout'=>'خروج',
    'website'=>'سایت',
    'symbol'=>'نماد',
    'store_category'=>'انبار',
    'currency'=>'واحد پول',
    'select'=>'انتخاب',
    'setting'=>'تنظیمات',
    'messages'=>'پیام ها',
    'manager'=>'مدیریت',
    'token'=>'access token',
    'domain'=>'دامنه',
    'update'=>'بروز رسانی',
    'sub-menu'=>'زیر منو',
    'print'=>'چاپ',
    'icon'=>'آیکون',
    'manual-address'=>'آدرس دستی',
    'parent'=>'والد',
    'self'=>'خودم',
    'address'=>'آدرس',
    'subject'=>'موضوع',
    'answer'=>'جواب',
    'name'=>'نام',
    'customer'=>'مشتری',
    'first-name'=>'نام',
    'last-name'=>'نام خانوادگی',
    'full-name'=>'نام و نام خانوادگی',
    'role'=>'نقش',
    'username'=>'نام کاربری',
    'password'=>'کلمه عبور',
    'info'=>'اطلاعات',
    'info-another'=>'اطلاعات بیشتر',
    'about-me'=>'درباره من',
    'no-role'=>'بدون نقش',
    'accessory'=>'دسترسی',
    'new-password'=>'کلمه عبور جدید',
    'current-password'=>'کلمه عبور فعلی',
    'price'=>'قیمت',
    'period'=>'دوره زمانی',
    'attributes'=>'ویژگی ها',
    'terminal'=>'ترمینال',
    'status'=>'وضعیت',
    'google-map-longitude'=>'طول نقضه گوگل',
    'google-map-latitude'=>'عرض نقشه گوگل',
    'google-map-link'=>'آدرس نقشه گوگل',
    'view'=>'تعداد بازدید',
    'like'=>'تعداد لایک',
    'star'=>'رتبه',
    'phone'=>'تلفن',
    'manufacturer_data'=>'ایجاد کننده',
    'author'=>'نویسنده',
    'publisher'=>'ناشر',
    'canonical'=>'لینک مرجع',
    'robots'=>'موتور جستجو',
    'android'=>'برنامه نویس موبایل',
    'senior-developer'=>'برنامه نویس ارشد',
    'database-analyst'=>'تحلیلگر دیتابیس',
    'market-finder'=>'بازار یاب',
    'the-secretary'=>'منشی',
    'accountants'=>'حسابدار',
    'graphic designer'=>'گرافیست',
    'janitor'=>'آبدار چی',
    'professor'=>'استاد',
    'members'=>'کارمندان',
    'project-manager'=>'مدیر پروژه',
    'contractors'=>'پیمانکار',
    'security-expert'=>'کارشناس امنیت',
    'inspector'=>'بازرس',
    'agent'=>'نماینده',
    'graphic-designer'=>'گرافیست',
    'search'=>'جستجو',
    'copy-right'=>'متن کپی رایت',
    'title-website'=>'نام سایت',
    'country'=>'کشور',
    'category'=>'دسته بندی',
    'file'=>'فایل',
    'download-file'=>'فایل دانلود',
    'ability'=>'توانایی',
    'columns'=>'ستون ها',
    'column'=>'ستون',
    'categories'=>'دسته بندی ها',
    'fax'=>'فکس',
    'iran'=>'ایران',
    'arabic'=>'عربستان',
    'german'=>'آلمان',
    'time_start'=>'ساعت شروع',
    'date_start'=>'تاریخ شروع',
    'time_end'=>'ساعت پایان',
    'date_end'=>'تاریخ پایان',
    'week'=>'روز های هفته',
    'week-services'=>'مشخص کردن زمان های هفته',
    'date'=>'تاریخ و ساعت',
    'date-services'=>'مشخص کردن تاریخ',
    'saturday'=>'شنیه',
    'sunday'=>'یکشنبه',
    'monday'=>'دوشنبه',
    'tuesday'=>'سه شنبه',
    'wednesday'=>'چهار شنبه',
    'thursday'=>'پنج شنبه',
    'friday'=>'جمعه',
    'event_place'=>'محل برگزاری',
    'capacity'=>'ظرفیت',
    'templates'=>'قالب ها',
    'total_hour'=>'تعداد کل ساعات',
    'leader'=>'سر گروه',
    'leaders'=>'سر گروه ها',
    'prerequisites'=>'پیش نیاز ها',
    'employer'=>'کارفرما',
    'seeker'=>'کارجو',
    'welcome_text'=>'متن خوش آمد گویی',
    'tanks_text'=>'ممنون بخاطره استقاده از وب ابلیکیشن ما!',
];
