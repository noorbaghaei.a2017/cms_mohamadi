@extends('core::layout.panel')
@section('pageTitle', 'داشبورد')
@section('content')
    <div class="row-col">
        <div class="col-lg b-r">
           @include('core::layout.access-information')
            @include('core::layout.log-information')

        </div>
    </div>
@endsection
