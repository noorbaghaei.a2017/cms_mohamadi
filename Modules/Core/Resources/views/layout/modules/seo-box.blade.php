@if(is_null($seo))

    <div class="box-header">
        <h2>{{__('cms.seo')}}</h2>
        <small>{{__('cms.seo-services')}}</small>
    </div>
    <div class="box-divider m-a-0"></div>
    <div class="box-body p-v-md">
        <div class="row row-sm">
            <div class="form-group row">
                <div class="col-sm-12">
                    <label for="title-seo" class="form-control-label">{{__('cms.title')}} </label>
                    <input type="text" name="title-seo" value="{{old('title-seo')}}" class="form-control" id="title-seo" autocomplete="off">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <label for="author-seo" class="form-control-label">{{__('cms.author')}} </label>
                    <input type="text" name="author-seo" class="form-control" value="{{old('author-seo')}}" id="author-seo" autocomplete="off">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <label for="publisher-seo" class="form-control-label">{{__('cms.publisher')}} </label>
                    <input type="text" name="publisher-seo" class="form-control" value="{{old('publisher-seo')}}" id="publisher-seo" autocomplete="off">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <label for="robots-seo" class="form-control-label">{{__('cms.robots')}} </label>
                <select dir="rtl" class="form-control"  id="robots-seo" name="robots[]" required multiple>
                    @foreach(config('cms.seo.robots') as $key=>$item)

                        <option value="{{$item}}">{{$item}}</option>

                    @endforeach
                </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <label for="canonical-seo" class="form-control-label">{{__('cms.canonical')}} </label>
                    <input type="text" name="canonical-seo" class="form-control" value="{{old('canonical-seo')}}" id="canonical-seo" autocomplete="off">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <label for="keyword-seo" class="form-control-label">{{__('cms.keyword')}} </label>
                    <input type="text" name="keyword-seo" class="form-control" value="{{old('keyword-seo')}}" id="keyword-seo" autocomplete="off">
                </div>


            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <label for="description-seo" class="form-control-label">{{__('cms.description')}} </label>
                    <input type="text" name="description-seo" class="form-control" value="{{old('description-seo')}}" id="description-seo" autocomplete="off">
                </div>
            </div>

        </div>
    </div>
@else
    <div class="box-header">
        <h2>{{__('cms.seo')}}</h2>
        <small>{{__('cms.seo-services')}}</small>
    </div>
    <div class="box-divider m-a-0"></div>
    <div class="box-body p-v-md">
        <div class="row row-sm">
            <div class="form-group row">
                <div class="col-sm-12">
                    <label for="title-seo" class="form-control-label">{{__('cms.title')}} </label>
                    <input type="text"  value="{{is_null($seo->title) ? "" : $seo->title}}" name="title-seo" class="form-control" id="title" autocomplete="off">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <label for="author-seo" class="form-control-label">{{__('cms.author')}} </label>
                    <input type="text" name="author-seo" class="form-control" value="{{is_null($seo->author) ? "" : $seo->author}}" id="author-seo" autocomplete="off">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <label for="publisher-seo" class="form-control-label">{{__('cms.publisher')}} </label>
                    <input type="text" name="publisher-seo" class="form-control" value="{{is_null($seo->publisher) ? "" : $seo->publisher}}" id="publisher-seo" autocomplete="off">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <label for="robots-seo" class="form-control-label">{{__('cms.robots')}} </label>

                    <select dir="rtl" class="form-control"  id="robots-seo" name="robots[]" required multiple>
                        @foreach(config('cms.seo.robots') as $key=>$item)

                            @if(isset($seo->robots))
                                <option value="{{$item}}"  {{in_array($item,json_decode($seo->robots)) ? "selected": ""}}>{{$item}}</option>

                                @else

                                <option value="{{$item}}">{{$item}}</option>

                            @endif

                        @endforeach
                    </select>

                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <label for="canonical-seo" class="form-control-label">{{__('cms.canonical')}} </label>
                    <input type="text" name="canonical-seo" class="form-control" value="{{is_null($seo->canonical) ? "" : $seo->canonical}}" id="canonical-seo" autocomplete="off">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <label for="keyword-seo" class="form-control-label">{{__('cms.keyword')}} </label>
                    <input type="text"  value="{{is_null($seo->keyword) ? "" : $seo->keyword}}"  name="keyword-seo" class="form-control" id="keyword" autocomplete="off">
                </div>


            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <label for="description-seo" class="form-control-label">{{__('cms.description')}} </label>
                    <input type="text"  value="{{is_null($seo->description) ? "" : $seo->description}}"  name="description-seo" class="form-control" id="description" autocomplete="off">
                </div>
            </div>
        </div>
    </div>
@endif





