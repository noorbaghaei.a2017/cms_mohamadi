<div class="padding">
    <div class="row m-b">

        @can('article-list')
        @if(hasModule('Article'))
        <div class="col-sm-6 col-xs-12">
            <div class="box">
                <div class="box-header light lt">
                    <h3> مقالات</h3>

                </div>

                <table class="table table-striped b-t">
                    <thead>
                    <tr>
                        <th>{{__('cms.title')}} </th>
                        <th>{{__('cms.excerpt')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($lastArticles as $article)
                    <tr>


                    <td>{{$article->title}} </td>
                    <td>{{$article->excerpt}} </td>



                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @endif
        @endcan

            @can('product-list')
            @if(hasModule('Product'))
        <div class="col-sm-6 col-xs-12">
            <div class="box">
                <div class="box-header light lt">
                    <h3>محصولات  </h3>
                </div>
                <table class="table table-striped b-t">
                    <thead>
                    <tr>
                        <th>{{__('cms.title')}} </th>
                        <th>{{__('cms.excerpt')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($lastProducts as $product)
                        <tr>


                            <td>{{$product->title}} </td>
                            <td>{{$product->excerpt}} </td>



                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
                @endif
                @endcan


    </div>
    <div class="row m-b">

        @can('information-list')
        @if(hasModule('Information'))
            <div class="col-sm-6 col-xs-12">
                <div class="box">
                    <div class="box-header light lt">
                        <h3> اخبار</h3>
                    </div>
                    <table class="table table-striped b-t">
                        <thead>
                        <tr>
                            <th>{{__('cms.title')}} </th>
                            <th>{{__('cms.excerpt')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($lastInformations as $information)
                            <tr>


                                <td>{{$information->title}} </td>
                                <td>{{$information->excerpt}} </td>



                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        @endif
            @endcan


                @can('user-list')
        @if(hasModule('User'))
        <div class="col-sm-6 col-xs-12">
            <div class="box">
                <div class="box-header light lt">
                    <h3> کاربران</h3>
                </div>
                <table class="table table-striped b-t">
                    <thead>
                    <tr>
                        <th>{{__('cms.full-name')}} </th>
                        <th>{{__('cms.email')}}</th>
                        <th>{{__('cms.role')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($lastUsers as $user)
                        <tr>


                            <td>{{fullName($user->first_name,$user->last_name)}} </td>
                            <td>{{$user->email}} </td>
                            <td>{{__('cms.'.$user->RoleName)}} </td>



                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
        @endif

            @endcan

    </div>
</div>
