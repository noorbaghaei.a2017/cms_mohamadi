@extends('core::layout.panel')
@section('pageTitle', 'مشاهده قالب')
@section('content')
    <div class="padding">
        <div class="row">
            @foreach($items as $item)
            <div class="col-md-12">
                <div class="box p-a-xs">
                    <div class="row">
                        <div class="col-md-5">
                            <a href="#">

                                    <img style="width: 400px;height: auto" src="{{asset('template/template/junior/junior.png')}}" alt="" class="img-responsive">

                            </a>
                        </div>
                        <div class="col-md-7">
                            <div style="padding-top: 35px">
                                <h6 style="padding-top: 35px"> عنوان   : </h6>
                                <h4 style="padding-top: 35px">    {{$item->title}}</h4>
                            </div>


                            <div>
                                <h6 style="padding-top: 35px"> توضیحات  : </h6>
                                <p>    {{$item->description}}</p>
                            </div>
                            <div>

                               @if(!$item->status)

                                    <a href="" class="btn btn-success">فعال</a>
                                    <a href="" class="btn btn-default" >غیر فعال</a>
                                @else
                                    <a href="" class="btn btn-default" >فعال</a>
                                    <a href="" class="btn btn-danger">غیر فعال</a>
                                @endif
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            @endforeach
        </div>
        </div>
    @endsection
