
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h2>{{__('cms.search')}} </h2>

            </div>
            <div class="box-divider m-a-0"></div>
            <div class="box-body">
                @include('core::layout.alert-danger')
                <form role="form" method="post" action="{{route('search.contact')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">

                        <div class="col-sm-3">
                            <label for="name" class="form-control-label">{{__('cms.name')}}  </label>
                            <input type="text" value="{{isset($request->name) ? $request->name : ""}}" name="name" class="form-control" id="name" >
                        </div>

                        <div class="col-sm-3">
                            <label for="last_name" class="form-control-label">{{__('cms.last_name')}}  </label>
                            <input type="text" value="{{isset($request->last_name) ? $request->last_name : ""}}" name="last_name" class="form-control" id="last_name" >
                        </div>

                        <div class="col-sm-3">
                            <label for="email" class="form-control-label">{{__('cms.email')}}  </label>
                            <input type="text" value="{{isset($request->email) ? $request->email : ""}}" name="email" class="form-control" id="email" >
                        </div>


                    </div>

                    <div class="form-group row m-t-md">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary btn-sm text-sm">{{__('cms.search')}} </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

