<?php

return [
    "text-create"=>' با استفاده از فرم زیر میتوانید ارز جدید اضافه کنید.',
    "text-edit"=>' با استفاده از فرم زیر میتوانید ارز خود را ویرایش کنید.',
    "store"=>"ثبت با موفقیت انجام شد.",
    "delete"=>"حذف با موفقیت انجام شد",
    "update"=>"بروز رسانی با موفقیت انجام شد",
    "index"=>"لیست ارز ها",
    "singular"=>"ارز",
    "collect"=>"ارز ها",
    "permission"=>[
        "currency-full-access"=>"دسترسی کامل به ارز",
        "currency-delete"=>"حذف ارز",
        "currency-create"=>"ایجاد ارز",
        "currency-edit"=>"ویرایش ارز",
    ]
];
