<?php


namespace Modules\Core\Helper\Trades;


trait TimeAttribute
{

    public  function getTimeCreateAttribute(){

        return isset($this->created_at) ? $this->created_at->format('d M') : "";
    }
    public  function getAgoTimeUpdateAttribute(){

        return isset($this->updated_at) ? $this->updated_at->ago() : "";
    }
    public  function getAgoTimeAttribute(){

        return isset($this->created_at) ? $this->created_at->ago() : "";
    }


    public  function getMonthYearAttribute(){

        return isset($this->created_at) ?  $this->created_at->format('d M') : "";
    }
}
