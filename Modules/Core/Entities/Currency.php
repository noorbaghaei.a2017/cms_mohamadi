<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Helper\Trades\TimeAttribute;

class Currency extends Model
{
    use TimeAttribute;
    protected $fillable = ['title','symbol'];

    public function getRouteKeyName()
    {
        return multiRouteKey();
    }

}
