<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = ['name','last_name','telephone','subject','email','message','token'];

    public function getRouteKeyName()
    {
        return multiRouteKey();
    }

    public  function getAgoTimeAttribute(){

        return $this->created_at->ago();
    }
}
