<?php

namespace Modules\Sms\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Sms\Entities\Sms;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('permissions')->whereModel(Sms::class)->delete();

        Permission::create(['name'=>'sms-list','model'=>Sms::class,'created_at'=>now()]);
        Permission::create(['name'=>'sms-create','model'=>Sms::class,'created_at'=>now()]);
        Permission::create(['name'=>'sms-edit','model'=>Sms::class,'created_at'=>now()]);
        Permission::create(['name'=>'sms-delete','model'=>Sms::class,'created_at'=>now()]);
    }
}
