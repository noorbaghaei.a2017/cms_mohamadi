@include('core::layout.modules.index',[

    'title'=>__('questions::questions.index'),
    'items'=>$items,
    'parent'=>'questions',
    'model'=>'questions',
    'directory'=>'questions',
    'collect'=>__('questions::questions.collect'),
    'singular'=>__('questions::questions.singular'),
    'create_route'=>['name'=>'questions.create'],
    'edit_route'=>['name'=>'questions.edit','name_param'=>'question'],
    'destroy_route'=>['name'=>'questions.destroy','name_param'=>'question'],
     'search_route'=>true,
    'datatable'=>[
    __('cms.title')=>'title',
   __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
    __('cms.title')=>'title',
    __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],


])

