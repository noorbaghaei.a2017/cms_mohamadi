<?php
return [
    "text-create"=>"you can create your member",
    "text-edit"=>"you can edit your member",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"members list",
    "singular"=>"member",
    "collect"=>"members",
    "permission"=>[
        "member-full-access"=>"member full access",
        "member-list"=>"members list",
        "member-delete"=>"member delete",
        "member-create"=>"member create",
        "member-edit"=>"edit member",
    ]
];
