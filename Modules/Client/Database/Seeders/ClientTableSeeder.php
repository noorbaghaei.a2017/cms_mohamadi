<?php

namespace Modules\Client\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Modules\Client\Entities\ClientRole;
use Modules\Core\Entities\User;

class ClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clients')->delete();

        $clients=[

            [
                'name' => 'Amin Nourbaghaei',
                'first_name' => 'امین',
                'last_name' => 'نوربقایی',
                'email' => 'noorbaghaei.a2017@gmail.com',
                'password' => Hash::make('12345Qwert12345'),
                'mobile' => '9195995044',
                'country' => '+98',
                'code' => rand(1000,9999),
                'username' => 'amin',
                'token' => tokenGenerate(),
                'avatar' => null,
                'status' => 1,
                'two_step' => null,
                'role' => ClientRole::whereTitle('user')->first()->id,
                'created_at' => now()
            ]
        ];

        DB::table('clients')->insert($clients);

    }
}
