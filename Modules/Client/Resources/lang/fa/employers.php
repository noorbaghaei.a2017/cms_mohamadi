<?php

return [
    "store"=>"ثبت با موفقیت انجام شد.",
    "delete"=>"حذف با موفقیت انجام شد",
    "update"=>"بروز رسانی با موفقیت انجام شد",
    "index"=>"لیست کافرمایان ",
    "singular"=>"کارفرما",
    "collect"=>"کافرمایان",
    "permission"=>[
        "employer-full-access"=>"دسترسی کامل به کافرمایان",
        "employer-list"=>"لیست کافرمایان",
        "employer-delete"=>"حذف کارفرما",
        "employer-create"=>"ایجاد کارفرما",
        "employer-edit"=>"ویرایش کافرمایان",
    ]
];
