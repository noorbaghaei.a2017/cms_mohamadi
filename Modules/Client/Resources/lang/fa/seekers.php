<?php

return [
    "store"=>"ثبت با موفقیت انجام شد.",
    "delete"=>"حذف با موفقیت انجام شد",
    "update"=>"بروز رسانی با موفقیت انجام شد",
    "index"=>"لیست کارجویان ",
    "singular"=>"کارجو",
    "collect"=>"کارجویان",
    "permission"=>[
        "seeker-full-access"=>"دسترسی کامل به کارجویان",
        "seeker-list"=>"لیست کارجویان",
        "seeker-delete"=>"حذف کارجو",
        "seeker-create"=>"ایجاد کارجو",
        "seeker-edit"=>"ویرایش کارجویان",
    ]
];
