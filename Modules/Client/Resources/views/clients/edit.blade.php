@extends('core::layout.panel')
@section('pageTitle', __('client::clients.client-edit'))
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box p-a-xs">
                    <div class="row">
                        <div class="col-md-5">
                            <a href="#">
                                @if(!$item->Hasmedia('images') && statusUrl(findAvatar($item->email,200))===200)
                                    <img style="height: auto" src="{{findAvatar($item->email,200)}}" alt="" >

                                @elseif(statusUrl(findAvatar($item->email,200))!==200)

                                    <img style="width: 300px;height: auto" src="{{asset('img/no-img.gif')}}">
                                @else

                                    <img style="width: 400px;height: auto" src="{{$item->getFirstMediaUrl('images')}}" alt="" class="img-responsive">


                                @endif


                            </a>
                        </div>
                        <div class="col-md-7">
                            <div style="padding-top: 35px">
                                <h6 style="padding-top: 35px"> {{__('cms.full-name')}}  : </h6>
                                <h4 style="padding-top: 35px">    {{fullName($item->firstname,$item->lastname)}}</h4>
                            </div>
                            <div>
                                <h6 style="padding-top: 35px"> {{__('cms.email')}} : </h6>
                                <p>    {{$item->email}}</p>
                            </div>

                            <div>
                                <h6 style="padding-top: 35px"> {{__('cms.mobile')}}  : </h6>
                                <p>    {{$item->mobile}}</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    @include('core::layout.alert-danger')
                    <div class="box-header">
                        <div class="pull-left">
                            <small>
                                با استفاده از فرم زیر میتوانید کاربر مورد نظر را مشاهده کنید.
                            </small>
                        </div>
                        <a onclick="window.print()" class="btn btn-primary btn-sm text-sm text-white pull-right">{{__('cms.print')}} </a>
                    </div>
                    <br>
                    <br>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <form action="{{route('clients.update', ['client' => $item->token])}}" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" value="{{$item->token}}" name="token">
                            {{method_field('PATCH')}}
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="firstname" class="form-control-label">{{__('cms.first-name')}}</label>
                                    <input type="text" value="{{$item->firstname}}" name="firstname" class="form-control" id="firstname" >
                                </div>
                                <div class="col-sm-3">
                                    <label for="lastname" class="form-control-label">{{__('cms.last-name')}} </label>
                                    <input type="text" value="{{$item->lastname}}" name="lastname" class="form-control" id="lastname" >
                                </div>
                                <div class="col-sm-3">
                                    <label for="mobile" class="form-control-label">{{__('cms.mobile')}}</label>
                                    <input  type="number" value="{{$item->mobile}}" name="mobile" class="form-control" id="mobile" >
                                </div>
                                <div class="col-sm-3">
                                    <label for="title" class="form-control-label">{{__('cms.thumbnail')}} </label>
                                    @include('core::layout.load-single-image')
                                </div>

                            </div>
                            <div class="row form-group">
                                <div class="col-sm-3">
                                    <label for="username" class="form-control-label">{{__('cms.username')}}</label>
                                    <input type="text" value="{{$item->username}}" name="username" class="form-control" id="username" >
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="email" class="form-control-label">{{__('cms.email')}} </label>
                                    <input type="text" value="{{$item->email}}" name="email" class="form-control" id="email" >

                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="password" class="form-control-label">{{__('cms.password')}} </label>
                                    <input type="text" name="password" class="form-control" id="password">

                                </div>
                            </div>

                            <div class="form-group row m-t-md">
                                <div class="col-sm-12">
                                    <input type="submit"  class="btn btn-success btn-sm text-sm" value="{{__('cms.update')}}">
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



@section('heads')

    <link href="{{asset('assets/css/validate/screen.css')}}" rel="stylesheet"/>

@endsection()

@section('scripts')


    <script src="{{asset('assets/scripts/validate/jquery.validate.js')}}"></script>


    <script>


        $().ready(function() {
            // validate the comment form when it is submitted
            $("#commentForm").validate();

            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                    firstname: {
                        required: true
                    },
                    lastname: {
                        required: true
                    },
                    mobile: {
                        required: true
                    },
                    role: {
                        required: true
                    },


                },
                messages: {
                    firstname:"نام الزامی است",
                    lastname: "نام خانوادگی  لزامی است",
                    mobile: "موبایل  الزامی است",
                    role: "نقش  الزامی است",
                }
            });


            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });
        });
    </script>

@endsection
