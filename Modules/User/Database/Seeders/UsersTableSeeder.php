<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('users')->delete();


        $user=[
            [
                'first_name'=>"امین",
                'last_name'=>"نوربقایی",
                'username'=>"nourbaghaei",
                'name'=>"amin",
                'mobile'=>"9195995044",
                'country' => '+98',
                'token'=>Str::random(),
                "email"=>"noorbaghaei.a2017@gmail.com",
                "password"=>Hash::make('secret'),
                "created_at"=>now()
            ],
            [
                'first_name'=>"شرکت",
                'last_name'=>"سیگما",
                'username'=>"company",
                'name'=>"sigma",
                'mobile'=>"9195995050",
                'country' => '+98',
                'token'=>Str::random(),
                "email"=>"sigma@gmail.com",
                "password"=>Hash::make('secret'),
                "created_at"=>now()
            ]
        ];


        DB::table('users')->insert($user);

    }
}
