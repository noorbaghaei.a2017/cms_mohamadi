@include('core::layout.modules.index',[

    'title'=>__('service::advantages.index'),
    'items'=>$items,
    'parent'=>'service',
    'model'=>'advantage',
    'directory'=>'advantages',
    'collect'=>__('service::advantages.collect'),
    'singular'=>__('service::advantages.singular'),
    'create_route'=>['name'=>'advantages.create'],
    'edit_route'=>['name'=>'advantages.edit','name_param'=>'advantage'],
    'destroy_route'=>['name'=>'advantages.destroy','name_param'=>'advantage'],
     'search_route'=>true,
    'datatable'=>[
    __('cms.title')=>'title',
    __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
    __('cms.title')=>'title',
     __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],


])
