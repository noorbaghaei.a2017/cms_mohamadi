<?php

namespace Modules\Service\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Mockery\Exception;
use Modules\Core\Entities\Category;
use Modules\Core\Http\Requests\CategoryRequest;
use Modules\Question\Entities\Question;
use Modules\Question\Http\Requests\QuestionRequest;
use Modules\Service\Entities\Service;
use Modules\Service\Http\Requests\ServiceRequest;
use Spatie\MediaLibrary\Models\Media;

class ServiceController extends Controller
{
    protected $entity;

    public function __construct()
    {
        $this->entity=new Service();

        $this->middleware('permission:service-list')->only('index');
        $this->middleware('permission:service-create')->only(['create','store']);
        $this->middleware('permission:service-edit' )->only(['edit','update']);
        $this->middleware('permission:service-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity->latest()->paginate(config('cms.paginate'));
            return view('service::services.index',compact('items'));
        }catch (\Exception $exception){
            return abort('500');
        }

    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
                !isset($request->title)
            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));
                return view('service::services.index',compact('items'));
            }
            $items=$this->entity
                ->where("title",trim($request->title))
                ->paginate(config('cms.paginate'));
            return view('service::services.index',compact('items','request'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $categories=Category::latest()->where('model',Service::class)->get();
        $parent_services=$this->entity->latest()->whereParent(0)->get();
        return view('service::services.create',compact('parent_services','categories'));
    }

    /**
     * Store a newly created resource in storage.
     * @param ServiceRequest $request
     * @return Response
     */
    public function store(ServiceRequest $request)
    {
        try {

            $parent=-1;
            if($request->input('parent')!==-1){
                $parent=$this->entity->whereToken($request->input('parent'))->first();
            }

            $this->entity->user=auth('web')->user()->id;
            $this->entity->title=$request->input('title');
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->category=Category::whereToken($request->input('category'))->first()->id;
            $this->entity->parent=($request->input('parent')==-1) ? 0: $parent->id;
            $this->entity->icon=$request->input('icon');
            $this->entity->text=$request->input('text');
            $this->entity->order=orderInfo($request->input('order'));
            $this->entity->token=tokenGenerate();


            $saved=$this->entity->save();

            $this->entity->seo()->create([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>json_encode($request->input('robots')),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);

            $this->entity->analyzer()->create();


            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!$saved){
                return redirect()->back()->with('error',__('service::services.error'));
            }else{
                return redirect(route("services.index"))->with('message',__('service::services.store'));
            }


        }catch (Exception $exception){
            return abort('500');
        }
    }


    /**
     * Show the form for editing the specified resource.
     * @param $token
     * @return Response
     */
    public function edit($token)
    {
        try {
            $categories=Category::latest()->where('model',Service::class)->get();
            $parent_services=$this->entity->latest()->whereParent(0)->get();
            $item=$this->entity->whereToken($token)->firstOrFail();
            return view('service::services.edit',compact('item','categories','parent_services'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param $token
     * @return void
     */
    public function update(ServiceRequest $request, $token)
    {
        try {
            $parent=-1;
            if($request->input('parent')!==-1){
                $parent=$this->entity->whereToken($request->input('parent'))->first();
            }

            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $updated=$this->entity->update([
                "user"=>auth('web')->user()->id,
                "title"=>$request->input('title'),
                "slug"=>null,
                "icon"=>$request->input('icon'),
                "category"=>Category::whereToken($request->input('category'))->first()->id,
                "parent"=>($request->input('parent')==-1) ? 0: $parent->id,
                "excerpt"=>$request->input('excerpt'),
                "text"=>$request->input('text'),
                "order"=>orderInfo($request->input('order')),
            ]);
            $this->entity->seo()->update([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>json_encode($request->input('robots')),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);

            $this->entity->replicate();
            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!$updated){
                return redirect()->back()->with('error',__('service::services.error'));
            }else{
                return redirect(route("services.index"))->with('message',__('service::services.update'));
            }


        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param $token
     * @return void
     */
    public function destroy($token)
    {
        try {

            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            destroyMedia($this->entity,config('cms.collection-image'));
            $deleted=$this->entity->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('service::services.error'));
            }else{
                return redirect(route("services.index"))->with('message',__('service::services.delete'));
            }


        }catch (\Exception $exception){
            return abort('500');
        }
    }


    public  function gallery(Request $request,$token){
        try {
            $item=$this->entity->whereToken($token)->first();
            $medias= $item->getMedia(config('cms.collection-images'));
            return view('service::services.gallery',compact('item','medias'));
        }catch (\Exception $exception){
            return abort('500');
        }

    }

    public  function galleryStore(Request $request,$token){
        try {

            $item=$this->entity->whereToken($token)->first();
            if($request->has('gallery')){
                $item->addMedia($request->file('gallery'))->toMediaCollection(config('cms.collection-images'));
                return redirect(route("services.index"))->with('message',__('service::services.store'));
            }

        }catch (\Exception $exception){
            return dd($exception->getMessage());
            return abort('500');
        }
    }

    public  function galleryDestroy(Request $request,$media){
        try {
            Media::find($media)->delete();

            return redirect(route("services.index"))->with('message',__('service::services.store'));

        }catch (\Exception $exception){
            return dd($exception->getMessage());
            return abort('500');
        }
    }


    public  function question($token){
        try {

            $item=$this->entity->whereToken($token)->first();
            $items=$item->questions;

            return view('service::questions.index',compact('items','token'));

        }catch (\Exception $exception){
            return dd($exception->getMessage());
        }
    }

    public  function questionCreate($token){
        try {

            return view('service::questions.create',compact('token'));

        }catch (\Exception $exception){
            return dd($exception->getMessage());
        }
    }
    public  function questionStore(QuestionRequest $request,$token){
        try {

            $this->entity=$this->entity->whereToken($token)->first();
            $saved=$this->entity->questions()->create([
                'user'=>auth('web')->user()->id,
                'title'=>$request->input('title'),
                'order'=>orderInfo($request->input('order')),
                'answer'=>$request->input('answer'),
                'token'=>tokenGenerate()
            ]);
            if(!$saved){
                return redirect()->back()->with('error',__('service::servces.error'));
            }else{
                return redirect(route("services.index"))->with('message',__('service::services.store'));
            }

        }catch (\Exception $exception){
            return dd($exception->getMessage());
        }
    }

    public  function questionEdit(Request $request,$article,$question){
        try {
            $token=$article;
            $item=Question::whereToken($question)->first();

            return view('service::questions.edit',compact('token','item'));

        }catch (\Exception $exception){
            return dd($exception->getMessage());
        }
    }
    public  function questionUpdate(QuestionRequest $request,$token){
        try {

            $question=Question::whereToken($token)->first();
            $updated=$question->update([
                'title'=>$request->input('title'),
                'order'=>orderInfo($request->input('order')),
                'answer'=>$request->input('answer')
            ]);
            if(!$updated){
                return redirect()->back()->with('error',__('service::services.error'));
            }else{
                return redirect(route("services.index"))->with('message',__('service::services.store'));
            }

        }catch (\Exception $exception){
            return dd($exception->getMessage());
        }
    }
    public  function questionDestroy(Request $request,$token){
        try {
            $question=Question::whereToken($token)->first();
            $deleted=$question->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('service::services.error'));
            }else{
                return redirect(route("services.index"))->with('message',__('service::services.store'));
            }

        }catch (\Exception $exception){
            return dd($exception->getMessage());
        }
    }


    public  function categories(){

        try {

            $items=Category::latest()->where('model',Service::class)->get();
            return view('service::categories.index',compact('items'));
        }catch (\Exception $exception){
            return dd($exception->getMessage());
        }
    }

    public  function categoryCreate(){
        try {
            $parent_categories=Category::latest()->whereParent(0)->where('model',Download::class)->get();
            return view('service::categories.create',compact('parent_categories'));

        }catch (\Exception $exception){
            return dd($exception->getMessage());
        }
    }

    public  function categoryStore(CategoryRequest $request){
        try {

            $parent=-1;
            if($request->input('parent')!==-1){
                $parent=Category::whereToken($request->input('parent'))->first();
            }

            $saved=Category::create([
                'user'=>auth('web')->user()->id,
                'title'=>$request->input('title'),
                'symbol'=>$request->input('symbol'),
                'icon'=>$request->input('icon'),
                'excerpt'=>$request->input('excerpt'),
                'model'=>Service::class,
                'parent'=>($request->input('parent')==-1) ? 0: $parent->id,
                'order'=>orderInfo($request->input('order')),
                'token'=>tokenGenerate(),
            ]);
            if(!$saved){
                return redirect()->back()->with('error',__('core::categories.error'));
            }else{
                return redirect(route("service.categories"))->with('message',__('core::categories.store'));
            }

        }catch (\Exception $exception){
            return dd($exception->getMessage());
        }
    }

    public  function CategoryEdit(Request $request,$token){
        try {

            $item=Category::whereToken($token)->first();
            $parent_categories=Category::latest()->whereParent(0)->where('model',Service::class)->get();

            return view('service::categories.edit',compact('item','parent_categories'));

        }catch (\Exception $exception){
            return dd($exception->getMessage());
        }
    }
    public  function categoryUpdate(Request $request,$token){
        try {


            $parent=-1;
            if($request->input('parent')!==-1){
                $parent=Category::whereToken($request->input('parent'))->first();
            }

            $category=Category::whereToken($token)->first();
            $updated=$category->update([
                'title'=>$request->input('title'),
                'symbol'=>$request->input('symbol'),
                'order'=>orderInfo($request->input('order')),
                'icon'=>$request->input('answer'),
                'excerpt'=>$request->input('excerpt'),
                'parent'=>($request->input('parent')==-1) ? 0: $parent->id,
            ]);
            if(!$updated){
                return redirect()->back()->with('error',__('service::downloads.error'));
            }else{
                return redirect(route("service.categories"))->with('message',__('core::categories.store'));
            }

        }catch (\Exception $exception){
            return dd($exception->getMessage());
        }
    }
}
