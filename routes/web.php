<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

Route::group(['prefix' => LaravelLocalization::setLocale(),'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]], function()
{
    Route::get('/', 'HomeController@website')->name('front.website');
    Route::get('/generate/site-map', 'HomeController@generateSiteMap')->name('generate.sitemap');
    Route::get('/articles', 'HomeController@articles')->name('articles');
    Route::get('/courses', 'HomeController@courses')->name('courses');
    Route::get('/races', 'HomeController@races')->name('races');
    Route::get('/articles/{article}', 'HomeController@singleArticle')->name('articles.single');
    Route::get('/courses/{course}', 'HomeController@singleCourse')->name('courses.single');
    Route::get('/races/{race}', 'HomeController@singleRace')->name('races.single');
    Route::get('/informations', 'HomeController@informations')->name('informations');
    Route::get('/informations/{information}', 'HomeController@singleInformation')->name('informations.single');
    Route::get('/portfolios', 'HomeController@portfolios')->name('portfolios');
    Route::get('/portfolios/{portfolio}', 'HomeController@singlePortfolio')->name('portfolios.single');
    Route::get('/products', 'HomeController@products')->name('products');
    Route::get('/contact-us', 'HomeController@contactUs')->name('contact-us');
    Route::get('/about-us', 'HomeController@aboutUs')->name('about-us');
    Route::get('/downloads', 'HomeController@downloads')->name('downloads');
    Route::get('/events', 'HomeController@events')->name('events');
    Route::get('/products/{product}', 'HomeController@singleProduct')->name('products.single');
    Route::get('/products/categories/{category}', 'HomeController@categoriesProduct')->name('categories.product');
    Route::get('/members/{member}', 'HomeController@singleMember')->name('members.single');
    Route::get('/page/{page}', 'HomeController@page')->name('page');
    Route::get('/panel/user/logout', 'HomeController@logout')->name('client.logout.panel');

    Route::group(["prefix"=>"user"],function() {
        Route::get('/panel', 'HomeController@panel')->name('client.dashboard')->middleware('auth:client');
        Route::get('/cart', 'HomeController@cart')->name('client.cart')->middleware('auth:client');
    });



});

Route::get('/send/email/{id}', 'NotifyController@index');




