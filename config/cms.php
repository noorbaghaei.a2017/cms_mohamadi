<?php

return [

    'version'=>1.5,
    'paginate'=>10,
    'prefix-admin'=>'dashboard',
    'icon'=>[
        'add'=>' fa fa-plus ',
        'role'=>' fa fa-user ',
        'detail'=>' fa fa-eye ',
        'delete'=>' fa fa-trash-o ',
        'edit'=>' fa fa-edit ',
        'gallery'=>' fa fa-image ',
        'questions'=>' fa fa-question',
        'categories'=>'fa fa-folder-open-o',
        'leaders'=>'fa fa-mortar-board',
    ],
    'country'=>[
        'iran'=>'ir',
        'arabic'=>'ar',
        'german'=>'de',

    ],
    'seo'=>[
        'robots'=>[
            'index','follow','noindex','nofollow','noarchive','nosnippet','all','none','noimageindex','notranslate'
        ]
    ],
    'collection-image'=>'images',
    'collection-images'=>'gallery',
    'collection-download'=>'download',
];
